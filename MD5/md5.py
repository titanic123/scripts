#writes out the md5 check sum of a string into a text file

import hashlib

read_file = "string.txt"
f = open(read_file, 'r')
write_file = "output.txt"
z = open(write_file, 'w')

md5_sum = (hashlib.md5(read_file.encode('utf-8')).hexdigest())

with z as writer:
    writer.writelines(md5_sum)

f.close()
z.close()

