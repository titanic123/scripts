#This is a web scraper prototype

import re
import requests
from bs4 import BeautifulSoup

source = requests.get('https://realpython.com/python-while-loop/').text

soup = BeautifulSoup(source, 'lxml')

#internal websites
internal_links = []
#external websites
external_links = []

for link in soup.find_all('a'):
    try:
        if not link.attrs['href'].startswith('#'):
            if link.attrs['hfef'].startswith('https://realpython.com') or link.attrs['href'].startswith('/'):
                internal_links.append(link.attrs['href'])
            else:
                external_links.append(link.attrs['href'])
    except:
        pass

print('There are {} internal links and {} external links on this page.'.format(len(internal_links), len(external_links)))
print(len(internal_links))
