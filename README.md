# Web projects and vulnerability testing

Repository currently contains:

## Vulnerable PHP login
Vulnerable website for SQL injection and XSS vulnerability proof of concept.

- style.css: stylesheet for the website
- index.php: login page
- mysql_table.sql: script for MySQL database

SQL Injection
- http://localhost/?username=admin'%20or%20'1'='1
- http://localhost/?username=admin'%20or%20''='
- http://localhost/index.php?id=1%20or%201=1

XSS:
- http://localhost/index.php?username=admin%3Cscript%3Ealert('Hi%20there!')%3C/script%3E

## Web Spider for Google Search Engine Optimisation
I wrote this tool to analyse the effect to Google Page rank algorithm based on the number of links a website has.
The tool looks at the total number of links and counts how many are internal, how many are external, and 
out of the external links it counts how many have the no follow attribute and prints the stats into a CSV file.
This wa designed to help a friend working at a Web company with a task pertaining SEO.

## MD5
This tool takes a file and outputs the MD5 sum of it on a text file.